export class EUDGCExpiredError extends Error {
  constructor() {
    super('Certificate expired.');
  }
}
export class EUDGCExpiresAfterDSCError extends Error {
  constructor() {
    super('Certificate expires after DSC error');
  }
}
export class EUDGCIssuedBeforeDSCError extends Error {
  constructor() {
    super('Certificate issued  before DSC error');
  }
}
export class InvalidDSCSignatureError extends Error {
  constructor() {
    super('Invalid DSC signature error.');
  }
}
export class InvalidEUDGCertificate extends Error {
  constructor() {
    super('Invalid european digital green certificate.');
  }
}
export class InvalidEUDGCSignatureError extends Error {
  constructor() {
    super('Invalid eudgc kid.');
  }
}
export class UnsupportedCoseAlgorithmError extends Error {
  constructor() {
    super('Unsupported cose algorithm.');
  }
}
export class UnsupportedEUDGCCertificateType extends Error {
  constructor() {
    super('Invalid eudgc certificate type.');
  }
}
export class UnsupportedEUDGCStatus extends Error {
  constructor() {
    super('Unsupported european digital green certificate status.');
  }
}
