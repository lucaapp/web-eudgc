export type EuDigitalGreenIssuerCertificate = {
  kid: string;
  country: string;
  rawData: string;
  signature: string;
  timestamp: string;
  thumbprint: string;
  certificateType: string;
};

export type EuDigitalGreenCertificateContent = Partial<{
  sc: string;
  ci: string;
  co: string;
  dn: number;
  dt: string;
  du: string;
  df: string;
  is: string;
  ma: string;
  mp: string;
  sd: number;
  tg: string;
  vp: string;
  tr: string;
  tt: string;
}>;

export type EuDigitalGreenCertificateName = {
  fn: string;
  gn: string;
  fnt: string;
  gnt: string;
};

export type EuDigitalGreenCertificate = {
  dob: string;
  ver: '1.0.0' | '1.0.3';
  nam: EuDigitalGreenCertificateName;
  r?: EuDigitalGreenCertificateContent[];
  t?: EuDigitalGreenCertificateContent[];
  v?: EuDigitalGreenCertificateContent[];
};
