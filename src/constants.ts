export const BASE_45_REGEX = /^[a-zA-Z0-9+\-*%$/.: ]+$/g;
export enum EUDigitalGreenCertificateTestType {
  PCR = 'LP6464-4',
  FAST = 'LP217198-3',
}
export enum EUDigitalGreenCertificateTestResult {
  POSITIVE = '260373001',
  NEGATIVE = '260415000',
}

export enum EuDigitalGreenCertificateType {
  TESTED = 'TESTED',
  RECOVERED = 'RECOVERED',
  VACCINATION = 'VACCINATION',
}
