import moment from 'moment';
import * as cbor from 'cbor';
import { inflate } from 'pako';
import * as cose from 'cosette';
import Ajv2020 from 'ajv/dist/2020';
import addFormats from 'ajv-formats';
import * as x509 from '@peculiar/x509';
import { decode as decodeBase45 } from 'base45';

import {
  bytesToHex,
  uint8ArrayToBytes,
  VERIFY_EC_SHA256_IEEE_SIGNATURE,
} from '@lucaapp/crypto';

// Types
import {
  EuDigitalGreenCertificate,
  EuDigitalGreenCertificateContent,
  EuDigitalGreenIssuerCertificate,
} from './types';

// Constants
import {
  BASE_45_REGEX,
  EUDigitalGreenCertificateTestResult,
  EUDigitalGreenCertificateTestType,
  EuDigitalGreenCertificateType,
} from './constants';

// Errors
import {
  EUDGCExpiredError,
  InvalidEUDGCertificate,
  UnsupportedEUDGCStatus,
  EUDGCIssuedBeforeDSCError,
  EUDGCExpiresAfterDSCError,
  InvalidEUDGCSignatureError,
  UnsupportedCoseAlgorithmError,
  UnsupportedEUDGCCertificateType,
} from './errors';

import schema from './DCC.combined-schema.json';

// For sanity check a QR Code can have at most 4296 alpha-numeric characters
// https://www.keyence.com/ss/products/auto_id/codereader/basic_2d/qr.jsp
const MAXIMUM_DATA_CAPACITY = 4296;

// Weisstein Length: https://stackoverflow.com/a/42752673/6915365
// Magic number: https://esdiscuss.org/topic/log10e-mystery-constant
const numberLength = (n: number) =>
  // tslint:disable-next-line:no-bitwise
  ((Math.log(Math.abs(n) + 1) * 0.43429448190325176) | 0) + 1;

export function isEUDigitalGreenCertificate(data: string): boolean {
  return data.startsWith('HC1:');
}

export function getCertificateType(
  certificate: EuDigitalGreenCertificate
): EuDigitalGreenCertificateType | null {
  if (certificate.r) {
    return EuDigitalGreenCertificateType.RECOVERED;
  }
  if (certificate.v) {
    return EuDigitalGreenCertificateType.VACCINATION;
  }
  if (certificate.t) {
    return EuDigitalGreenCertificateType.TESTED;
  }

  return null;
}

export function checkCertificateVaccinationValid(
  certificateVerification: EuDigitalGreenCertificateContent
): boolean {
  const now = moment();

  if (certificateVerification.sd && certificateVerification.dn) {
    return (
      certificateVerification.sd < certificateVerification.dn ||
      (certificateVerification.sd === certificateVerification.dn &&
        (certificateVerification.sd > 2 ||
          now.isAfter(moment(certificateVerification.dt).add(15, 'days'))))
    );
  }

  if (certificateVerification.du && certificateVerification.df) {
    return (
      now.isBefore(moment(certificateVerification.du)) &&
      now.isAfter(moment(certificateVerification.df))
    );
  }

  if (
    certificateVerification.tr &&
    certificateVerification.tt &&
    certificateVerification.sc &&
    certificateVerification.tr[EUDigitalGreenCertificateTestResult.NEGATIVE]
  ) {
    // @ts-ignore
    if (certificateVerification.tt[EUDigitalGreenCertificateTestType.PCR]) {
      return now.isBefore(moment(certificateVerification.sc).add(2, 'days'));
    }
    // @ts-ignore
    if (certificateVerification.tt[EUDigitalGreenCertificateTestType.FAST]) {
      return now.isBefore(moment(certificateVerification.sc).add(1, 'days'));
    }

    return false;
  }

  return false;
}

export async function parseEUDigitalGreenCertificate(
  rawCertificateData: string,
  getIssuer: (kid: string) => Promise<EuDigitalGreenIssuerCertificate>
): Promise<EuDigitalGreenCertificate> {
  if (!isEUDigitalGreenCertificate(rawCertificateData)) {
    throw new InvalidEUDGCertificate();
  }

  if (rawCertificateData.length > MAXIMUM_DATA_CAPACITY) {
    throw new InvalidEUDGCertificate();
  }

  const encodedData = rawCertificateData.slice(4);
  if (encodedData.search(BASE_45_REGEX) === -1) {
    throw new InvalidEUDGCertificate();
  }

  const b45decodedData = decodeBase45(encodedData);
  const decompressedData = inflate(b45decodedData);
  const dataBuffer = cbor.decodeFirstSync(decompressedData);
  const decodedCert = cbor.decodeFirstSync(dataBuffer.value[2]);

  const iss = decodedCert.get(1);
  if (typeof iss !== 'string' || iss.length !== 2) {
    throw new InvalidEUDGCertificate();
  }

  const exp = decodedCert.get(4);
  if (typeof exp !== 'number' || numberLength(exp) !== 10) {
    throw new InvalidEUDGCertificate();
  }

  const iat = decodedCert.get(6);
  if (typeof iat !== 'number' || numberLength(iat) !== 10) {
    throw new InvalidEUDGCertificate();
  }

  const signature = bytesToHex(uint8ArrayToBytes(dataBuffer.value[3]));

  const euDigitalGreenCertificate: EuDigitalGreenCertificate = decodedCert
    .get(-260)
    .get(1);

  const ajv = new Ajv2020();
  ajv.addKeyword('valueset-uri');
  // @ts-ignore
  addFormats(ajv);
  const validate = ajv.compile(schema);
  if (!validate(euDigitalGreenCertificate) as any) {
    throw new InvalidEUDGCertificate();
  }

  // eslint-disable-next-line no-undef
  const kidRaw = Buffer.from(
    cbor
      .decodeFirstSync(dataBuffer.value[0])
      .get(cose.common.HeaderParameters.kid) ||
      dataBuffer.value[1].get(cose.common.HeaderParameters.kid)
  );

  if (kidRaw.length !== 8) {
    throw new InvalidEUDGCSignatureError();
  }
  const kid = kidRaw.toString('base64');

  const issuer = await getIssuer(kid);
  const cert = new x509.X509Certificate(issuer.rawData);
  const asn1CodedPublicKey = uint8ArrayToBytes(
    new Uint8Array(cert.publicKey.rawData)
  );
  const publicKey = bytesToHex(asn1CodedPublicKey).slice(
    52,
    asn1CodedPublicKey.length * 2
  );

  const data = bytesToHex(
    uint8ArrayToBytes(
      new Uint8Array(
        cbor.encode([
          'Signature1',
          dataBuffer.value[0],
          new ArrayBuffer(0),
          dataBuffer.value[2],
        ])
      )
    )
  );

  if (!VERIFY_EC_SHA256_IEEE_SIGNATURE(publicKey, data, signature)) {
    throw new InvalidEUDGCSignatureError();
  }

  const alg = cbor
    .decodeFirstSync(dataBuffer.value[0])
    .get(cose.common.HeaderParameters.alg);

  // Check if is valid algorithm
  if (![-7, -37].includes(alg)) {
    throw new UnsupportedCoseAlgorithmError();
  }

  if (
    (alg === -7 && signature.length !== 128) ||
    (alg === -37 && signature.length !== 512)
  ) {
    throw new InvalidEUDGCertificate();
  }

  const expirationDate = decodedCert.get(4);
  const issuedAtDate = decodedCert.get(6);

  if (moment.unix(issuedAtDate).isBefore(moment(cert.notBefore))) {
    throw new EUDGCIssuedBeforeDSCError();
  }

  if (moment.unix(expirationDate).isAfter(moment(cert.notAfter))) {
    throw new EUDGCExpiresAfterDSCError();
  }

  if (expirationDate && moment.unix(expirationDate).isBefore(moment())) {
    throw new EUDGCExpiredError();
  }

  if (getCertificateType(euDigitalGreenCertificate) === null) {
    throw new UnsupportedEUDGCCertificateType();
  }

  if (
    euDigitalGreenCertificate.v &&
    !checkCertificateVaccinationValid(euDigitalGreenCertificate.v[0])
  ) {
    throw new UnsupportedEUDGCStatus();
  }

  if (
    euDigitalGreenCertificate.t &&
    !checkCertificateVaccinationValid(euDigitalGreenCertificate.t[0])
  ) {
    throw new UnsupportedEUDGCStatus();
  }

  if (
    euDigitalGreenCertificate.r &&
    !checkCertificateVaccinationValid(euDigitalGreenCertificate.r[0])
  ) {
    throw new UnsupportedEUDGCStatus();
  }

  return euDigitalGreenCertificate;
}
