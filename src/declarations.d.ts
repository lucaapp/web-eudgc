declare module 'cbor-web' {
  const module: any;
  export = module;
}

interface WebCrypto extends Crypto {
  ensureSecure(): Promise<any>;
}

declare module 'isomorphic-webcrypto' {
  const module: WebCrypto;
  export = module;
}
