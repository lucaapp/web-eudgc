import { isEUDigitalGreenCertificate } from './parse';

describe('parse', () => {
  describe('isEUDigitalGreenCertificate', () => {
    it('returns true if a strings starts with HC1:', () => {
      expect(isEUDigitalGreenCertificate('HC1:')).toBe(true);
    });
  });
});
