# Luca EUDGC

[luca](https://luca-app.de) ensures a data protection-compliant, decentralized
encryption of your data, undertakes the obligation to record contact data for
events and gastronomy, relieves the health authorities through digital, lean,
and integrated processes to enable efficient and complete tracing.

This repository contains the source code of the luca EUDGC package. The Luca
EUDGC package contains all functionalities related to the EUDGC (European digital green certificates) which are needed
by our different web services. (You can find our webservices [here](https://gitlab.com/lucaapp/web/)).

### Example
To test the package functionalities please check out the example/run.js file. (Please run
```yarn``` in the example folder)

## Changelog

An overview of all releases can be found
[here](https://gitlab.com/lucaapp/web-eudgc/-/blob/master/CHANGELOG.md).

## Issues & Support

Please [create an issue](https://gitlab.com/lucaapp/web-eudgc/-/issues) for
suggestions or problems related to this application. For general questions,
please check out our [FAQ](https://www.luca-app.de/faq/) or contact our support
team at [hello@luca-app.de](mailto:hello@luca-app.de).

## License

The luca EUDGC package is Free Software (Open Source) and is distributed with a
number of components with compatible licenses.

```
SPDX-License-Identifier: Apache-2.0

SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
```

For details see

- [license file](https://gitlab.com/lucaapp/web-eudgc/-/blob/master/README.md)
