### 1.0.2 (2022-01-09)
* Bump version of @lucaapp/crypto from v4.0.0 to 4.0.1

### 1.0.1 (2021-12-20)
* Bump version of @peculiar/x509 from v1.3.0 to 1.6.1

### 1.0.0 (2021-12-17)
* initial public release
