#!groovy

BRANCH_NAME = env.BRANCH_NAME
BUILD_NUMBER = env.BUILD_NUMBER
BRANCH_NAME_ESCAPED = BRANCH_NAME.replaceAll('/', '-')
UNIQUE_TAG = "${BRANCH_NAME_ESCAPED}_build-${BUILD_NUMBER}"

void abortPreviousRunningBuilds() {
  echo 'Aborting previous builds'
  def jobname = env.JOB_NAME
  def buildnum = env.BUILD_NUMBER.toInteger()

  // get raw job from jenkins
  def job = Jenkins.instance.getItemByFullName(jobname)
  for (build in job.builds) {
    //ignore if it is not building
    if (!build.isBuilding()) {
      continue;
    }
    //check if the same number as currentBuild, if so skip
    if (buildnum == build.getNumber().toInteger()) {
      continue; println 'equals'
    }
    echo "Aborting previous build = ${build}"
    build.doStop()
  }
}

void updateSourceCode() {
  cleanWs()
  checkout scm
  // replace public registry references with private registries
  withCredentials([
      string(credentialsId: 'luca-docker-repository', variable: 'DOCKER_REPOSITORY'),
      string(credentialsId: 'luca-npm-registry', variable: 'NPM_REGISTRY')
    ]) {
      sh('./scripts/usePrivateRegistries.sh')
    }
}

Closure executeCIScript(String script) {
  return {
    node('docker') {
      def IMAGE_TAG = "${script.replace(".", "_")}_${UNIQUE_TAG}".toLowerCase();
      try {
        updateSourceCode()
        withCredentials([
          usernamePassword( credentialsId: 'jenkins-docker-public-registry',
                  usernameVariable:'DOCKER_PUBLIC_USERNAME',
                  passwordVariable:'DOCKER_PUBLIC_PASSWORD'),
          string(credentialsId: 'luca-docker-public-registry', variable: 'DOCKER_PUBLIC_REGISTRY'),
          string(credentialsId: 'luca-npm-auth', variable: 'NPM_CONFIG__AUTH'),
        ]) {
          sh('docker login -u=$DOCKER_PUBLIC_USERNAME -p=$DOCKER_PUBLIC_PASSWORD $DOCKER_PUBLIC_REGISTRY')
          sh("docker build -f ./docker/Dockerfile.ci --build-arg NPM_CONFIG__AUTH=$NPM_CONFIG__AUTH -t ${IMAGE_TAG} .")
          sh("docker run --rm -i ${IMAGE_TAG} ./ci/${script}")
        }
      } finally {
        sh("docker rmi ${IMAGE_TAG}")
        cleanWs()
      }
    }
  }
}

Closure executeSonarScript() {
  return {
    node('docker') {
      try {
        updateSourceCode()
        withSonarQubeEnv('sonarqube neXenio')
        {
          withCredentials([
          usernamePassword(credentialsId: 'luca-docker-auth', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD'),
          string(credentialsId: 'luca-docker-registry', variable: 'DOCKER_REGISTRY'),
          string(credentialsId: 'luca-npm-auth', variable: 'NPM_CONFIG__AUTH'),
        ]){
            def IMAGE_TAG = "sonar_${UNIQUE_TAG}".toLowerCase();
            sh('docker login -u=$DOCKER_USERNAME -p=$DOCKER_PASSWORD $DOCKER_REGISTRY')
            sh("docker build -f ./docker/Dockerfile.ci --build-arg NPM_CONFIG__AUTH=$NPM_CONFIG__AUTH -t ${IMAGE_TAG} .")
            sh("docker run \
              --rm \
              -v `pwd`/coverage:/app/coverage \
              -i ${IMAGE_TAG} \
              ./ci/unit.sh")
            if (env.BRANCH_NAME.startsWith('PR')) {

              sh("docker run \
                --rm \
                -e SONAR_HOST_URL=$SONAR_HOST_URL \
                -e SONAR_LOGIN=$SONAR_AUTH_TOKEN \
                -v `pwd`:/usr/src \
                -v /tmp/sonar_cache:/opt/sonar-scanner/.sonar/cache \
                $DOCKER_REGISTRY/tools/sonar-scanner:4 \
                  sonar-scanner \
                  -Dsonar.branch.name=${env.CHANGE_BRANCH} ")
            } else {
              // run branch analysis
              sh("docker run \
              --rm \
              -e SONAR_HOST_URL=$SONAR_HOST_URL \
              -e SONAR_LOGIN=$SONAR_AUTH_TOKEN \
              -v `pwd`:/usr/src \
              -v /tmp/sonar_cache:/opt/sonar-scanner/.sonar/cache \
              $DOCKER_REGISTRY/tools/sonar-scanner:4 \
                sonar-scanner \
                -Dsonar.branch.name=${BRANCH_NAME} ")
            }
          }
        }
      } finally {
        cleanWs()
      }
    }
  }
}

node {
  try {
    abortPreviousRunningBuilds()
    updateSourceCode()

    stage('Code Quality') {
      def steps = [:]
      steps['Sonar'] = executeSonarScript()
      steps['Audit'] = executeCIScript('audit.sh')
      steps['Linting'] = executeCIScript('linting.sh')
      parallel steps
    }

    currentBuild.result = 'SUCCESS'
  } catch(org.jenkinsci.plugins.workflow.steps.FlowInterruptedException err) {
    echo 'Script was aborted. ' + err.toString()
    currentBuild.result = 'ABORTED'
  } catch (err) {
    echo 'Script failed because of error: ' + err.toString()
    currentBuild.result = 'FAILURE'
  } finally {
    cleanWs()
  }
}
